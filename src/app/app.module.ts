import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UIModule } from 'xplat/web/feature/ui';
import { TramitesModule } from './modules/tramites/tramites.module';
import { InformateModule } from './modules/informate/informate.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    UIModule,
    TramitesModule,
    InformateModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
