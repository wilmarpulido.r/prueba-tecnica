import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardInformateComponent } from './card-informate.component';

describe('CardInformateComponent', () => {
  let component: CardInformateComponent;
  let fixture: ComponentFixture<CardInformateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardInformateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardInformateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
