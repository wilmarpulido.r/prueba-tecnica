import { Component, OnInit, Input } from '@angular/core';
import { CardInformateConfig } from '@wilmarcore/interfases';

@Component({
  selector: 'app-card-informate',
  templateUrl: './card-informate.component.html'
})
export class CardInformateComponent implements OnInit {

  @Input() public config: CardInformateConfig;

  constructor() { }

  ngOnInit(): void {
  }

}
