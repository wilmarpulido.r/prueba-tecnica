import { Component, OnInit } from '@angular/core';
import { LinkConfig } from '@wilmar/feature/ui';
import { CardInformateConfig } from '@wilmarcore/interfases';

import { CommonService } from '@wilmar/core/services';

@Component({
  selector: 'app-informate',
  templateUrl: './informate.component.html',
  providers: [CommonService]
})
export class InformateComponent implements OnInit {
  public linkInformate: LinkConfig;
  public arrCardsInformate: CardInformateConfig[];

  constructor(private commonService: CommonService) {}

  ngOnInit(): void {
    this._initValues();
    this.getInformate();
  }

  _initValues(): void {
    this.linkInformate = {
      txt: 'Ver otras noticias',
      link: '#',
      class: 'justify-content-center',
      iconClass: 'govco-icon-right-arrow'
    };
  }

  public getInformate(): void {
    this.commonService.getInformate().subscribe(
      result => {
        this.arrCardsInformate = result;
      },
      err => console.log(err)
    );
  }

}
