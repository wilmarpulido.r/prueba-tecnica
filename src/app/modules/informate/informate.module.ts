import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UIModule } from 'xplat/web/feature/ui';
import { HttpClientModule } from '@angular/common/http';
import { InformateComponent } from './informate.component';
import { CardInformateComponent } from './components/card-informate/card-informate.component';

@NgModule({
  declarations: [
    InformateComponent,
    CardInformateComponent
  ],
  imports: [
    CommonModule,
    UIModule,
    HttpClientModule
  ],
  exports: [
    InformateComponent
  ]
})
export class InformateModule {}
