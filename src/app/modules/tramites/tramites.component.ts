import { Component, OnInit } from '@angular/core';
import { InfoTramiteConfig } from '@wilmar/core/interfases';
import { LinkConfig } from '@wilmar/feature/ui';

import { CommonService } from '@wilmar/core/services';

@Component({
  selector: 'app-tramites',
  templateUrl: './tramites.component.html',
  providers: [CommonService]
})
export class TramitesComponent implements OnInit {
  public arrTramites: InfoTramiteConfig[];
  public linkTramites: LinkConfig;

  constructor(private commonService: CommonService) {}

  ngOnInit(): void {
    this._initValues();
    this.getTramites();
  }

  _initValues(): void {
    this.linkTramites = {
      txt: 'Conocer otros trámites',
      link: '#',
      class: 'justify-content-end',
      iconClass: 'govco-icon-right-arrow'
    };
  }

  public getTramites(): void {
    this.commonService.getTramites().subscribe(
      result => {
        this.arrTramites = result;
      },
      err => console.log(err)
    );
  }
}
