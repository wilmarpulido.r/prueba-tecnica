import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselTramiteComponent } from './carousel-tramite.component';

describe('CarouselTramiteComponent', () => {
  let component: CarouselTramiteComponent;
  let fixture: ComponentFixture<CarouselTramiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselTramiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselTramiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
