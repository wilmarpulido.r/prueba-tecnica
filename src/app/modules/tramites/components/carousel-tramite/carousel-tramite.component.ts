import { Component, OnInit, Input } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { InfoTramiteConfig } from '@wilmar/core/interfases';

import { CommonService } from '@wilmar/core/services';

@Component({
  selector: 'app-carousel-tramite',
  templateUrl: './carousel-tramite.component.html',
  providers: [CommonService]
})
export class CarouselTramiteComponent implements OnInit {

  public carouselOptions: OwlOptions;
  public arrTramites: InfoTramiteConfig[];

  constructor(private commonService: CommonService) {}

  ngOnInit(): void {
    this._initValues();
    this.getTramites();
  }

  _initValues(): void {
    this.carouselOptions = {
      loop: true,
      mouseDrag: false,
      touchDrag: false,
      pullDrag: false,
      dots: true,
      nav: false,
      navSpeed: 700,
      navText: ['', ''],
      responsive: {
        0: {
          items: 1
        }
      },
    };
  }

  public getTramites(): void {
    this.commonService.getTramites().subscribe(
      result => {
        this.arrTramites = result.filter(element => {
          return element.masUsado === true;
        });
      },
      err => console.log(err)
    );
  }
}
