import { Component, OnInit, Input } from '@angular/core';
import { InfoTramiteConfig } from '@wilmar/core/interfases';

@Component({
  selector: 'app-info-tramite',
  templateUrl: './info-tramite.component.html'
})
export class InfoTramiteComponent implements OnInit {

  @Input() public config: InfoTramiteConfig;

  constructor() {}

  ngOnInit(): void {}

}
