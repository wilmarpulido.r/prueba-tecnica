import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { UIModule } from 'xplat/web/feature/ui';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { TramitesComponent } from './tramites.component';
import { InfoTramiteComponent } from './components/info-tramite/info-tramite.component';
import { CarouselTramiteComponent } from './components/carousel-tramite/carousel-tramite.component';

@NgModule({
  declarations: [
    TramitesComponent,
    InfoTramiteComponent,
    CarouselTramiteComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    UIModule,
    CarouselModule
  ],
  exports: [
    TramitesComponent
  ]
})
export class TramitesModule {}
