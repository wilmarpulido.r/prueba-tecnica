# Prueba tecnica

Este proyecto es desarrollado con el Frameworks [Angular] usando su CLI en su versión 10.0.5 y generado por medio del comando `ng new nombre-proyecto`.

Para la creación de los componentes me apoye igualmente del CLI con el comando `ng g component nombre-componente`.

He creado la carpeta [xplat] donde esta todo el core del site. Encontraran constantes, interfases, servicios, los componentes UI globales y los estilos con una estructura `sass`.

## Servidor de desarrollo

Para levantar el proyecto lo primero que se debe hacer después de haberlo clonado del repositorio es correr el comando `npm i` para instalar todas las dependencias. Cuando la instalación termine, correr el comando `npm run start:dev` e ir al navegador a la ruta `http://localhost:4300/`. La aplicación se recargara automáticamente si se hacen cambios en los archivos `source`.

## Data

La data que se trae por servicio se encuentra alojada en [Firebase], una base de datos en tiempo real y 5 imágenes en el storage de dicha herramienta.