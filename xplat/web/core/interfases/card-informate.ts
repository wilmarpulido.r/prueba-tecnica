export interface CardInformateConfig {
  colClass?: string;
  href: string;
  urlImage: string;
  date: string;
  description: string;
}
