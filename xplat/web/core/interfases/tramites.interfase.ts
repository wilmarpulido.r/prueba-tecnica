export interface InfoTramiteConfig {
  title: string;
  entity: string;
  type: string;
  masUsado: boolean;
  onLine: boolean;
}
