import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Global } from '../constants/constants';

@Injectable()
export class CommonService {

  constructor(private http: HttpClient) {}

  getTramites(): Observable<any> {
    return this.http.get(Global.url + 'tramites.json',
        { headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  getInformate(): Observable<any> {
    return this.http.get(Global.url + 'informate.json',
        { headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }
}
