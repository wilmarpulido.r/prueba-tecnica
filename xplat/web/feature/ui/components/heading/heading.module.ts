import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { HeadingComponent } from './heading.component';

@NgModule({
  declarations: [HeadingComponent],
  exports: [HeadingComponent],
  imports: [CommonModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HeadingModule {}
