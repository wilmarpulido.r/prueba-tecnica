import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-heading',
  templateUrl: './heading.component.html'
})
export class HeadingComponent implements OnInit {

  @Input() text: string;

  constructor() {}

  ngOnInit(): void {}

}
