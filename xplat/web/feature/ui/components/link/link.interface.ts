export interface LinkConfig {
  txt: string;
  link: string;
  class: string;
  iconClass: string;
}
