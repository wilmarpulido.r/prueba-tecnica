import { Component, OnInit, Input } from '@angular/core';
import { LinkConfig } from './link.interface';

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html'
})
export class LinkComponent implements OnInit {

  @Input() public config: LinkConfig;

  constructor() {}

  ngOnInit(): void {}

}
