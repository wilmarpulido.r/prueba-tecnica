import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  public headerVariable: boolean;

  constructor() {
    this.headerVariable = false;
  }

  ngOnInit(): void {
  }

  @HostListener('document:scroll')
  public scrollfunction(): void {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      this.headerVariable = true;
    }
    else{
      this.headerVariable = false;
    }
  }

}
