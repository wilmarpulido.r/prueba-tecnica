import { CommonModule } from '@angular/common';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderModule } from './components/header';
import { FooterModule } from './components/footer';
import { LinkModule } from './components/link';
import { HeadingModule } from './components/heading';

const MODULES: any[] = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  HeaderModule,
  FooterModule,
  LinkModule,
  HeadingModule
];

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES],
  schemas: [NO_ERRORS_SCHEMA]
})

export class UIModule {}
