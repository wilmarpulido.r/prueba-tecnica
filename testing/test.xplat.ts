import { getTestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import 'core-js/es7/reflect';
import 'zone.js/dist/zone';
import 'zone.js/dist/zone-testing';

declare const require: any;

// web support only right now
const contextWeb: any = require.context('../xplat/web', true, /\.spec\.ts$/);
// And load the modules.
contextWeb.keys().map(contextWeb);
